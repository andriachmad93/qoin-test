package models

import (
	"fmt"
	"strings"
)

const (
	REMOVED_WHEN_DICE_TOP = 6
	MOVE_WHEN_DICE_TOP    = 1
)

var dice_top_side strings.Builder

type Game struct {
	Player                []Player
	Round                 int
	Numberofplayer        int
	Numberofdiceperplayer int
}

func (g *Game) DisplayRound() {
	fmt.Printf("Turn %d \n", g.Round)
}

func (g *Game) DisplayTopSideDice(title string) {
	fmt.Println(title)

	for i := 0; i < len(g.Player); i++ {

		fmt.Printf("Player %s : ", g.Player[i].Name)

		for ii := 0; ii < len(g.Player[i].GetDiceIn()); ii++ {
			dice_top_side.WriteString(fmt.Sprintf("%d \n", g.Player[i].GetDiceIn()[ii].GetTopSide()))
		}
	}

	fmt.Println(dice_top_side)
}

func (g *Game) DisplayWinner(player int) {
	fmt.Printf("Player %d \n", player)
}

func (g *Game) Start() {
	fmt.Printf("Player : %d, Dice : %d \n", g.Numberofplayer, g.Numberofdiceperplayer)

	for {
		g.Round++
		// dice_carry_forward := "1"

		for i := 0; i < len(g.Player); i++ {
			g.Player[i].Play()
		}

		g.DisplayRound()
		g.DisplayTopSideDice("Throw Dice")

		for ii := 0; ii < len(g.Player); ii++ {
			// temp_dice

			for iii := 0; iii < len(g.Player[ii].GetDiceIn()); iii++ {
				if g.Player[ii].GetDiceIn()[iii].GetTopSide() == REMOVED_WHEN_DICE_TOP {
					g.Player[ii].AddPoint(1)
					g.Player[ii].RemoveDice(iii)
				}

				if g.Player[ii].GetDiceIn()[iii].GetTopSide() == MOVE_WHEN_DICE_TOP {
					if g.Player[ii].GetPosition() == (g.Numberofplayer - 1) {
						g.Player[0].InsertDice(g.Player[ii].GetDiceIn())
						g.Player[ii].RemoveDice(iii)
					} else {
						g.Player[ii].RemoveDice(iii)
					}
				}
				break
			}
		}

		g.DisplayTopSideDice("Setelah Evaluasi")

		player_has_dice := g.Numberofplayer

		for iiii := 0; iiii < len(g.Player); iiii++ {
			if len(g.Player[iiii].GetDiceIn()) <= 0 {
				player_has_dice--
			}
		}

		if player_has_dice == 1 {
			g.GetWinner()
		}

		break
	}
}

func (g *Game) GetWinner() string {
	winner := ""
	highscore := 0

	for i := 0; i < len(g.Player); i++ {
		if g.Player[i].GetPoint() > highscore {
			highscore = g.Player[i].GetPoint()
			winner = g.Player[i].Name
		}
	}

	return winner
}
