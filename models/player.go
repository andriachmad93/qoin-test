package models

type Player struct {
	DiceIn   []Dice
	Name     string
	Position int
	Point    int
}

func (p *Player) SetPlayer(numberofdice int, position int, name string) {
	s := p.DiceIn

	p.Point = 0
	p.Position = position
	p.Name = name

	for i := 0; i < numberofdice; i++ {
		s = append(s, Dice{TopSide: i})
	}

	p.DiceIn = s
}

func (p *Player) GetDiceIn() []Dice {
	return p.DiceIn
}

func (p *Player) GetName() string {
	return p.Name
}

func (p *Player) GetPosition() int {
	return p.Position
}

func (p *Player) GetPoint() int {
	return p.Point
}

func (p *Player) AddPoint(point int) {
	p.Point += point
}

func (p *Player) Play() {
	for i := 0; i < len(p.DiceIn); i++ {
		p.DiceIn[i].Roll()
	}
}

func (p *Player) RemoveDice(key int) {
	s := p.DiceIn
	s = append(s[:key], s[key+1:]...)

	p.DiceIn = s
}

func (p *Player) InsertDice(dice []Dice) {
	s := p.DiceIn

	for i := 0; i < len(dice); i++ {
		s = append(s, dice...)
	}

	p.DiceIn = s
}
