package models

import (
	"math/rand"
	"time"
)

/**
* Author Andri
 */

/**
* type Dice int TopSide
 */
type Dice struct {
	TopSide int
}

func (d *Dice) SetDice(topside int) {
	d.SetTopSide(topside)
}

func (d *Dice) Roll() int {
	rand.Seed(time.Now().UnixNano())

	d.TopSide = rand.Intn((6 - 1 + 1) + 1)

	return d.TopSide
}

func (d *Dice) GetTopSide() int {
	return d.TopSide
}

func (d *Dice) SetTopSide(topside int) Dice {
	return Dice{TopSide: topside}
}
